<?php

use Illuminate\Support\Facades\Route;

Route::name('pengiriman-status.')->prefix('pengiriman-status')->group(function () {
    Route::post('/pengiriman-status-import', 'PengirimianStatusController@import')->name('pengiriman-status-import')->middleware('auth:api');
    Route::delete('/destroyAll', 'PengirimianStatusController@destroyAll')->name('destroy-all')->middleware('auth:api');
});
Route::apiResource('pengiriman-status', 'PengirimianStatusController')->middleware('auth:api');
