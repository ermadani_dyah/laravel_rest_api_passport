<?php

use Illuminate\Support\Facades\Route;

Route::name('pelanggan.')->prefix('pelanggan')->group(function () {
    Route::post('/pelanggan-import', 'PelangganController@import')->name('pelanggan-import')->middleware('auth:api');
    Route::delete('/destroyAll', 'PelangganController@destroyAll')->name('destroy-all')->middleware('auth:api');
    Route::post('/{pelanggan}', 'PelangganController@update')->name('update-post')->middleware('auth:api');
});
Route::apiResource('pelanggan', 'PelangganController')->middleware('auth:api');
