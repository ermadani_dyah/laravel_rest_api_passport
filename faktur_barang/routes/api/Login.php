<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Route;

Route::name('login.')->prefix('login')->group(function () {
    Route::post('/login', 'LoginController@login')->name('login');
});
