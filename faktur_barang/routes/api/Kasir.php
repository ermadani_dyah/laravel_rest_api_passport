<?php

use Illuminate\Support\Facades\Route;

Route::name('kasir.')->prefix('kasir')->group(function () {
    Route::post('/kasir-import', 'KasirController@import')->name('kasir-import')->middleware('auth:api');
    Route::delete('/destroyAll', 'KasirController@destroyAll')->name('destroy-all')->middleware('auth:api');
    Route::post('/{kasir}', 'KasirController@update')->name('update-post')->middleware('auth:api');
    Route::get('/kasir-export-pdf', 'KasirController@exportPdf')->name('kasir-export-pdf')->middleware('auth:api');
    Route::get('/kasir-export-excel', 'KasirController@exportExcel')->name('kasir-export-excel')->middleware('auth:api');
});
Route::apiResource('kasir', 'KasirController')->middleware('auth:api');
