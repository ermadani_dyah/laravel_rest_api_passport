<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Route;

Route::name('register.')->prefix('register')->group(function () {
    Route::post('/register', 'RegisterController@register')->name('register');
});
