<?php

use Illuminate\Support\Facades\Route;

Route::name('tipe-barang.')->prefix('tipe-barang')->group(function () {
    Route::post('/tipe-barang-import', 'TipeBarangController@import')->name('tipe-barang-import')->middleware('auth:api');
    Route::delete('/destroyAll', 'TipeBarangController@destroyAll')->name('destroy-all')->middleware('auth:api');
});
Route::apiResource('tipe-barang', 'TipeBarangController')->middleware('auth:api');
