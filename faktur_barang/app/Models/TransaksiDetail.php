<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Transaksi;
use App\Models\Barang;
class TransaksiDetail extends Model
{
    use HasFactory;
    protected $table="transaksi_details";
    public function barangs()
    {
        return $this->belongsTo(Barang::class, "barang_id");
    }
    public function transaksis(){
    	return $this->hasMany(Transaksi::class, "transaksi_id");
    }
    protected $fillable = [
        'barang_id','transaksi_id','kuantitas'
    ]; 
}
