<?php

namespace App\Exports;

use App\Models\Kasir;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
class KasirExport implements FromCollection,WithHeadings
{
    public function collection()
    {
        return Kasir::all();
    }
    public function headings(): array
    {
        return [
            'id',
            'Nama Kasir',
            'Created at',
            'Updated at',
            'Foto',
        ];
    }
}
