<?php

namespace App\Exports;

use App\Models\Transaksi;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
class TransaksiExport implements FromCollection,WithHeadings,WithMapping,ShouldAutoSize
{
    protected $from, $to;

    function __construct($from, $to)
    {
        $this->from = $from;
        $this->to = $to;
    }
    public function collection()
    {
        $transaksi = Transaksi::with(['details.barangs','kasirs','pelanggans'])
        ->whereBetween('transaksis.tanggal_transaksi', [$this->from, $this->to])
        ->get();
        return $transaksi;
    }
    public function map($transaksi): array
    {  
        $result=
            [
                [
                    $transaksi->id,
                    $transaksi->tanggal_transaksi,
                    $transaksi->kasirs->nama_kasir,
                    $transaksi->pelanggans->nama_pelanggan,
                ]
            ];
            foreach($transaksi->details as $item){
                $result[] = [
                    "","","","",
                    $item->barangs->nama_barang,
                    "Rp ".number_format($item->barangs->harga,0,'','.').",00",
                    $item->kuantitas,
                    "Rp ".number_format($transaksi->total,0,'','.').",00"
                ];
            }
        return $result;

    }
    public function headings(): array
    {
        return [
            'id',
            'Tanggal Transaksi',
            'Kasir',
            'Pelanggan',
            'Barang',
            'Harga',
            'Kuantitas',
            'Total'
        ];
    }
}
