<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Barang;
use App\Models\Kasir;
use App\Models\Transaksi;
use App\Models\TransaksiDetail;
use App\Models\Pelanggan;
class UpdateTransaksi extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'kasir_id' => 'sometimes|required|numeric|exists:' . Kasir::class . ',id',
            'pelanggan_id' => 'sometimes|required|numeric|exists:' . Pelanggan::class . ',id',
            'barang_id' => 'sometimes|required|numeric|exists:' . Barang::class . ',id',
            'tanggal_transaksi' => 'sometimes|required|date',
            'kuantitas' => 'sometimes|required|numeric'
        ];
    }
}
