<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Barang;
use App\Models\Kasir;
use App\Models\Transaksi;
use App\Models\TransaksiDetail;
use App\Models\Pelanggan;
class StoreTransaksi extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'kasir_id' => 'required|numeric|exists:' . Kasir::class . ',id',
            'pelanggan_id' => 'required|numeric|exists:' . Pelanggan::class . ',id',
            'tanggal_transaksi' => 'required|date'
        ];
    }
}
