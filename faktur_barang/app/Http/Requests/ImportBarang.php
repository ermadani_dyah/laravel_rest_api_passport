<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\TipeBarang;
class ImportBarang extends FormRequest
{
    
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'data' => 'required|array',
            'data.*.tipe_barang' => 'required|string',
            'data.*.nama_barang' => 'required|string',
            'data.*.harga' => 'required|numeric',
            'data.*.gambar' => 'required'
        ];
    }
}
