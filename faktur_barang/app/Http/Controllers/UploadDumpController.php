<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UploadDump;
use Carbon\Carbon;
use App\Http\Requests\StoreUploadDump;
use App\Http\Requests\UpdateUploadDump;
class UploadDumpController extends Controller
{
    CONST FETCHED_ATTRIBUTE = [
        "file_name",
        "file_type",
        "file_siza",
        "category",
        "folder",
        "uploader_ip",
        "rel_path",
        "abs_path",
        "file"
    ];
    public function index()
    {
        $dumps = UploadDump::all();
        return response()->json(['data'=>$dumps], 200);
    }
    public function create()
    {
        //
    }
    public function store(StoreUploadDump $request)
    {
        $data = $request->only(self::FETCHED_ATTRIBUTE);
        $img = $request->file('file');
        $path = 'dump/'.$data['folder'].'/';
        $timestamp = Carbon::now()->unix();
        $name = $timestamp.'_'.$img->getClientOriginalName();
        $img->move($path, $name);

        $filename = $img->getClientOriginalName();
        $filetype = $img->getClientOriginalExtension();
        $filesize = $img->getMaxFilesize();
        $rel_path = $path.$name;
        $abs_path = $request->getSchemeAndHttpHost().'/'.$rel_path;
        $data['file_name'] = $filename;
        $data['file_type'] = $filetype;
        $data['file_siza'] = $filesize;
        $data['rel_path'] = $rel_path;
        $data['abs_path'] = $abs_path;
        $data['uploader_ip'] = $request->ip();
        $img = UploadDump::create($data);
        return response()->json([
            'success' => true,
            'message' => 'Data Berhasil Ditambah',
            'data'    => $data 
        ], 201);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
