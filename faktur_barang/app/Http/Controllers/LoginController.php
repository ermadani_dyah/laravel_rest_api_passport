<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\StoreLogin;
class LoginController extends Controller
{   
    CONST FETCHED_ATTRIBUTE = [
        "email",
        "password"
    ];
    public function login(StoreLogin $request)
    {
        $data = $request->only(self::FETCHED_ATTRIBUTE);
        $user = User::where('email', $data['password'])->first();

        if (!$user || !Hash::check($data['password'], $user->$data['password'])) {
            return response()->json([
                'success' => false,
                'message' => 'Login Failed!',
            ],401);
        }

        return response()->json([
            'success' => true,
            'message' => 'Login Success!',
            'data'    => $user,
            'token'   => $user->createToken('authToken')->accessToken    
        ],200);
    }
    
    public function logout(Request $request)
    {
        $removeToken = $request->user()->tokens()->delete();

        if($removeToken) {
            return response()->json([
                'success' => true,
                'message' => 'Logout Success!',  
            ]);
        }
    }
}