<?php

namespace App\Http\Controllers;
use App\Http\Requests\StorePengiriman;
use App\Http\Requests\UpdatePengiriman;
use App\Http\Requests\ImportPengiriman;
use App\Models\Pengiriman;
use App\Models\Kurir;
use App\Models\PengirimianStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
class PengirimanController extends Controller
{
    CONST FETCHED_ATTRIBUTE = [
        "kurir_id",
        "transaksi_id",
        "pengiriman_status_id",
        "tanggal_pengiriman",
    ];
    public function index()
    {
        $pengiriman = Pengiriman::with(['transaksi.pelanggans','kurir','status'])->get();
        return response()->json([
            'success' => true,
            'message' => 'List Data Pengiriman',
            'data'    => $pengiriman
        ], 200);

    }
    public function show(Pengiriman $pengiriman)
    {
        $pengiriman->load(['transaksi.pelanggans','kurir','status']);
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Pengiriman',
            'data'    => $pengiriman
        ], 200);
    }
    public function store(StorePengiriman $request)
    {
        $data = $request->only(self::FETCHED_ATTRIBUTE);
        $pengiriman = Pengiriman::create($data);
        if($pengiriman) {
            //jika data berhasil ditambah
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Ditambah',
                'data'    => $pengiriman
            ], 201);

        }else{
            //jika data gagal ditambah
            return response()->json([
                'success' => false,
                'message' => 'Data Gagal Ditambah',
            ], 409);
        }
    }
    public function update(UpdatePengiriman $request, Pengiriman $pengiriman)
    {
        $data = $request->only(self::FETCHED_ATTRIBUTE);
        $pengiriman->update($data);
        if($pengiriman) {
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Diupdate',
                'data'    => $pengiriman
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Data Gagal Diupdate!',
            ], 500);
        }
    }
    public function destroy(Pengiriman $pengiriman)
    {
        $pengiriman->delete();
        if($pengiriman){
            //data berhasil dihapus
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Dihapus',
            ], 200);
        }else{
            //data gagal dihapus
            return response()->json([
                'success' => false,
                'message' => 'Data Gagal Dihapus!',
            ], 500);
        }
    }
    public function destroyAll()
    {
        $pengiriman = Pengiriman::truncate();
        return response()->json([
            'success' => true,
            'message' => 'Data Berhasil Dihapus',
        ], 200);
    }
    public function import(ImportPengiriman $request)
    {
        $data = $request->data;
        $result = [];
        foreach ($data as $d) {
            $kurir = Kurir::where('nama_kurir', $d['kurir'])->first();
            $status = PengirimianStatus::where('nama_status', $d['pengiriman_status'])->first();
            $d['kurir_id'] = $kurir->id;
            $d['pengiriman_status_id'] = $status->id;
            $result[] = Pengiriman::create($d);
        }
        return response($result);
    }
}
