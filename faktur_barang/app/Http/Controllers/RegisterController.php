<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\StoreRegistrasi;
class RegisterController extends Controller
{   
    CONST FETCHED_ATTRIBUTE = [
        "name",
        "email",
        "password"
    ];
    public function register(StoreRegistrasi $request)
    {
        $data = $request->only(self::FETCHED_ATTRIBUTE);
        $data['password']=Hash::make($data['password']);
        $user = User::create([
            'name'      => $data['name'],
            'email'     => $data['email'],
            'password'  => Hash::make($data['password'])
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Register Success!',
            'data'    => $user  
        ]);
    }
}
