<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use Faker\Factory as Faker;
class KasirSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker::create('id_ID');
 
    	for($i = 1; $i <= 5; $i++){
 
    	      // insert data ke table pegawai menggunakan Faker
            DB::table('kasirs')->insert([
    			'nama_kasir' => $faker->name,
    			'foto' => $faker->image('public/dump/Kasir/',640,480, null, false)
    		]);
 
    	}
    }
}
