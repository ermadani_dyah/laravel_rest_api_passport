<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUploadDumpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('upload_dump', function (Blueprint $table) {
            $table->id();
            $table->string('file_name',255);
            $table->string('file_type',255);
            $table->integer('file_siza');
            $table->string('category',255);
            $table->string('folder',255);
            $table->string('uploader_ip',255);
            $table->string('rel_path',255);
            $table->string('abs_path',255);
            $table->tinyInteger('upload_status')->default(1);
            $table->tinyInteger('link_status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('upload_dump');
    }
}
