<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;			// <-- import Traits Laravel Passport

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable; // <-- gunakan Traits HasApiTokens
}