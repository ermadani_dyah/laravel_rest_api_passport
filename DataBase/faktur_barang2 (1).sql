-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 13, 2021 at 06:23 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `faktur_barang2`
--

-- --------------------------------------------------------

--
-- Table structure for table `barangs`
--

CREATE TABLE `barangs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type_barang_id` bigint(20) UNSIGNED NOT NULL,
  `nama_barang` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `barangs`
--

INSERT INTO `barangs` (`id`, `type_barang_id`, `nama_barang`, `harga`, `created_at`, `updated_at`) VALUES
(2, 3, 'Pisau', 3000, '2021-07-12 05:17:35', '2021-07-12 05:17:35'),
(3, 3, 'Garpu', 34000, '2021-07-12 20:47:10', '2021-07-12 20:47:10');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kasirs`
--

CREATE TABLE `kasirs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_kasir` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kasirs`
--

INSERT INTO `kasirs` (`id`, `nama_kasir`, `created_at`, `updated_at`) VALUES
(2, 'Norman', '2021-07-12 06:58:14', '2021-07-12 06:58:14');

-- --------------------------------------------------------

--
-- Table structure for table `kurirs`
--

CREATE TABLE `kurirs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_kurir` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kurirs`
--

INSERT INTO `kurirs` (`id`, `nama_kurir`, `created_at`, `updated_at`) VALUES
(2, 'J&T', '2021-07-12 07:09:12', '2021-07-12 07:12:37'),
(3, 'Si cepat', '2021-07-12 07:09:28', '2021-07-12 07:09:28');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_07_12_072751_create_tipe_barangs_table', 1),
(5, '2021_07_12_072831_create_barangs_table', 1),
(6, '2021_07_12_072856_create_pelanggans_table', 1),
(7, '2021_07_12_072923_create_kurirs_table', 1),
(8, '2021_07_12_072956_create_pengirimian_statuses_table', 1),
(9, '2021_07_12_073014_create_kasirs_table', 1),
(10, '2021_07_12_073049_create_transaksis_table', 1),
(11, '2021_07_12_073120_create_transaksi_details_table', 1),
(12, '2021_07_12_073141_create_pengirimen_table', 2),
(13, '2021_07_12_084121_add_pengiriman_status_id_to_pengirimen_table', 3),
(14, '2021_07_12_084504_add_pengiriman_status_to_pengirimen_table', 4),
(15, '2021_07_12_085431_add_drop_colom_id_to_pengirimian_statuses_table', 5),
(16, '2021_07_12_085612_add_colom_id_to_pengirimen_table', 5),
(17, '2021_07_12_090151_add_colom_name_id_to_pengirimian_statuses_table', 6),
(18, '2021_07_12_090427_add_forgent_key_to_pengirimen_table', 7),
(19, '2021_07_12_090748_add_colom_name_forgent_key_to_pengirimen_table', 8),
(20, '2021_07_12_091819_add_primary_key_to_pengirimian_statuses_table', 9),
(21, '2021_07_12_091905_add_forgent_key_name_id_to_pengirimen_table', 9);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pelanggans`
--

CREATE TABLE `pelanggans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_pelanggan` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pelanggans`
--

INSERT INTO `pelanggans` (`id`, `nama_pelanggan`, `created_at`, `updated_at`) VALUES
(2, 'Puja', '2021-07-12 07:26:43', '2021-07-12 07:26:43');

-- --------------------------------------------------------

--
-- Table structure for table `pengirimen`
--

CREATE TABLE `pengirimen` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kurir_id` bigint(20) UNSIGNED NOT NULL,
  `transaksi_id` bigint(20) UNSIGNED NOT NULL,
  `tanggal_pengiriman` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `pengiriman_status_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pengirimen`
--

INSERT INTO `pengirimen` (`id`, `kurir_id`, `transaksi_id`, `tanggal_pengiriman`, `created_at`, `updated_at`, `pengiriman_status_id`) VALUES
(1, 2, 1, '2021-07-01', NULL, NULL, 'R'),
(3, 3, 5, '2021-07-01', '2021-07-13 08:40:45', '2021-07-13 08:40:45', 'A'),
(4, 2, 5, '2021-07-09', '2021-07-13 08:50:15', '2021-07-13 08:50:15', 'R');

-- --------------------------------------------------------

--
-- Table structure for table `pengirimian_statuses`
--

CREATE TABLE `pengirimian_statuses` (
  `nama_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pengirimian_statuses`
--

INSERT INTO `pengirimian_statuses` (`nama_status`, `created_at`, `updated_at`, `id`) VALUES
('Jaemin', NULL, '2021-07-12 09:14:40', 'A'),
('Belum Dipacking', NULL, NULL, 'B'),
('BB', '2021-07-12 09:13:23', '2021-07-12 09:13:23', 'R');

-- --------------------------------------------------------

--
-- Table structure for table `tipe_barangs`
--

CREATE TABLE `tipe_barangs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tipe_barang` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tipe_barangs`
--

INSERT INTO `tipe_barangs` (`id`, `tipe_barang`, `created_at`, `updated_at`) VALUES
(2, 'Alat Masak', '2021-07-12 03:17:45', '2021-07-12 03:17:45'),
(3, 'Alat Makan', '2021-07-12 03:30:43', '2021-07-12 03:30:43');

-- --------------------------------------------------------

--
-- Table structure for table `transaksis`
--

CREATE TABLE `transaksis` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `total` bigint(20) NOT NULL,
  `tanggal_transaksi` date NOT NULL,
  `kasir_id` bigint(20) UNSIGNED NOT NULL,
  `pelanggan_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transaksis`
--

INSERT INTO `transaksis` (`id`, `total`, `tanggal_transaksi`, `kasir_id`, `pelanggan_id`, `created_at`, `updated_at`) VALUES
(1, 0, '2021-07-13', 2, 2, '2021-07-12 22:33:27', '2021-07-12 22:33:27'),
(2, 0, '2021-07-13', 2, 2, '2021-07-12 22:34:15', '2021-07-12 22:34:15'),
(3, 0, '2021-07-13', 2, 2, '2021-07-12 22:34:27', '2021-07-12 22:34:27'),
(5, 3000, '2021-07-17', 2, 2, '2021-07-12 22:59:41', '2021-07-12 22:59:41'),
(6, 34000, '2021-07-17', 2, 2, '2021-07-12 23:43:21', '2021-07-12 23:43:21'),
(7, 34000, '2021-07-15', 2, 2, '2021-07-13 05:40:45', '2021-07-13 05:40:45'),
(8, 3000, '2021-07-15', 2, 2, '2021-07-13 05:42:36', '2021-07-13 05:42:36');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_details`
--

CREATE TABLE `transaksi_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kuantitas` int(11) NOT NULL,
  `barang_id` bigint(20) UNSIGNED NOT NULL,
  `transaksi_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transaksi_details`
--

INSERT INTO `transaksi_details` (`id`, `kuantitas`, `barang_id`, `transaksi_id`, `created_at`, `updated_at`) VALUES
(2, 1, 2, 5, '2021-07-12 22:59:41', '2021-07-12 22:59:41'),
(3, 1, 3, 6, '2021-07-12 23:43:21', '2021-07-12 23:43:21'),
(7, 1, 3, 7, '2021-07-13 05:40:45', '2021-07-13 05:40:45'),
(8, 1, 2, 8, '2021-07-13 05:42:36', '2021-07-13 05:42:36');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barangs`
--
ALTER TABLE `barangs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `barangs_type_barang_id_foreign` (`type_barang_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `kasirs`
--
ALTER TABLE `kasirs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kurirs`
--
ALTER TABLE `kurirs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pelanggans`
--
ALTER TABLE `pelanggans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengirimen`
--
ALTER TABLE `pengirimen`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pengirimen_kurir_id_foreign` (`kurir_id`),
  ADD KEY `pengirimen_transaksi_id_foreign` (`transaksi_id`),
  ADD KEY `pengirimen_pengiriman_status_id_foreign` (`pengiriman_status_id`);

--
-- Indexes for table `pengirimian_statuses`
--
ALTER TABLE `pengirimian_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tipe_barangs`
--
ALTER TABLE `tipe_barangs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksis`
--
ALTER TABLE `transaksis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transaksis_kasir_id_foreign` (`kasir_id`),
  ADD KEY `transaksis_pelanggan_id_foreign` (`pelanggan_id`);

--
-- Indexes for table `transaksi_details`
--
ALTER TABLE `transaksi_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transaksi_details_transaksi_id_foreign` (`transaksi_id`),
  ADD KEY `transaksi_details_barang_id_foreign` (`barang_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barangs`
--
ALTER TABLE `barangs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kasirs`
--
ALTER TABLE `kasirs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kurirs`
--
ALTER TABLE `kurirs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `pelanggans`
--
ALTER TABLE `pelanggans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pengirimen`
--
ALTER TABLE `pengirimen`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tipe_barangs`
--
ALTER TABLE `tipe_barangs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `transaksis`
--
ALTER TABLE `transaksis`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `transaksi_details`
--
ALTER TABLE `transaksi_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `barangs`
--
ALTER TABLE `barangs`
  ADD CONSTRAINT `barangs_type_barang_id_foreign` FOREIGN KEY (`type_barang_id`) REFERENCES `tipe_barangs` (`id`);

--
-- Constraints for table `pengirimen`
--
ALTER TABLE `pengirimen`
  ADD CONSTRAINT `pengirimen_kurir_id_foreign` FOREIGN KEY (`kurir_id`) REFERENCES `kurirs` (`id`),
  ADD CONSTRAINT `pengirimen_pengiriman_status_id_foreign` FOREIGN KEY (`pengiriman_status_id`) REFERENCES `pengirimian_statuses` (`id`),
  ADD CONSTRAINT `pengirimen_transaksi_id_foreign` FOREIGN KEY (`transaksi_id`) REFERENCES `transaksis` (`id`);

--
-- Constraints for table `transaksis`
--
ALTER TABLE `transaksis`
  ADD CONSTRAINT `transaksis_kasir_id_foreign` FOREIGN KEY (`kasir_id`) REFERENCES `kasirs` (`id`),
  ADD CONSTRAINT `transaksis_pelanggan_id_foreign` FOREIGN KEY (`pelanggan_id`) REFERENCES `pelanggans` (`id`);

--
-- Constraints for table `transaksi_details`
--
ALTER TABLE `transaksi_details`
  ADD CONSTRAINT `transaksi_details_barang_id_foreign` FOREIGN KEY (`barang_id`) REFERENCES `barangs` (`id`),
  ADD CONSTRAINT `transaksi_details_transaksi_id_foreign` FOREIGN KEY (`transaksi_id`) REFERENCES `transaksis` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
